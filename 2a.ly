\version "2.18.2"
% -*- master: score.ly;

global = {
  \time 6/8
  \key g \major
  \clef bass
}

twoA = \relative c' {
  \global
  \compressMMRests
  b8( a g  e d 'b)
  d4.(~d4 cis8)
  c!( a fis e d fis)
  a4.(~a4 b8)
  c(-- a e g fis d')
  d(-- b fis a g fis \break
  e) d'( b cis b a
  d) fis,( e d c! a)
  g( b d e d b'
  d) b( a g4.)
  a,8( c e f e a
  c) a( g fis!4.)
  g8( fis f e d c
  b) a(  e' g4.) \break
  d,8( b' c \grace { b16 c } b4 a8) 
  g4.( g'8) r r 
  e(\downbow fis g b c b) 
  d,( e fis b c b)  
  c,( d e fis g e)  
  b( d e fis g d) 
  a( c e g e c') \break
  
  b( a8. e16 g4.) 
  a,8( g' e fis d b') 
  a( e8. d16 fis4.) 
  g8( b d b g d) 
  e( g b g e b) 
  c( e g) e( c a) 
  d,4 d''8(~d cis c) 
  \grace { b16( c } b8)( a g e d b') \break
 d4.(~d4 cis8)
  c!( a fis e d fis)
  a4.(~a4 b8)
  c(-- a e g fis d')
  d(-- b fis a g fis 
  e) d'( b cis b a
  d) fis,( e d c! a) \break
  g( b d e d b'
  d) b( a g4.)
  a,8( c e f e a
  c) a( g fis!4.)
  g8( fis f e d c
  b) a(  e' g4.) 
  d,8( b' c \grace { b16 c } b4 a8) 
  g4.( g'8) r r \bar "|."
}
