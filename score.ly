% -*- master: score.ly;
% -*- output: pdfs/;

\version "2.24.0"
#(set-default-paper-size "letter")
#(set-global-staff-size 19)


% -*- master: federicos_lament.ly;
\include "../defs.ly"

tempoStyleTextScript = {
  \once \override Score.TextScript.font-size = #1.4
}
smallerTextMark = {
  \once \override Score.TextMark.font-size = #0
}

global = {
  \partial 8 s8 
  \tempo "Tempo"
  \tag #'part {
    \override Score.SpacingSpanner.spacing-increment = #2.8 
    \override TextScript.staff-padding = #2.0
    \override Score.TextMark.padding = #2.0
    \override Score.MetronomeMark.padding = #4
  }
  
  \override Score.MetronomeMark.font-size = #1.4
  \override Score.TextMark.font-size = #1.4

  \set Score.rehearsalMarkFormatter = #format-mark-box-numbers
  \tag #'score { \override Score.SpacingSpanner.spacing-increment = #2 }
 
}



\header {
  title = "Fifteen Etudes, Op. 76"
  subtitle = "(with 2nd Violoncello accompaniment)"
  composer = "David Popper"
}

\include "1a.ly"
\include "1b.ly"
\include "2a.ly"
\include "2b.ly"
\layout {
  indent = 0\in

}

\paper {
  right-margin = 0.30\in
  left-margin = 0.30\in
  system-system-spacing =
    #'((basic-distance . 18) 
    (minimum-distance . 8)
    (padding . 1)
    (stretchability . 60))       
  
  bookpart-level-page-numbering = ##t
  %print-page-number = ##t
  %print-first-page-number = ##t
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  oddFooterMarkup = \markup {
    \fill-line {
      \if \should-print-page-number
      \fromproperty #'page:page-number-string
    }
  }
  evenFooterMarkup = \oddFooterMarkup
}

\layout {
  
  #(layout-set-staff-size 18)
  indent = 0.5\in  

  \context {
    \Staff
    keepAliveInterfaces = #'()
  }
}

\book {

  \paper {
          %print-page-number = ##f
    %print-first-page-number = ##f
    indent = 0\mm
    bookTitleMarkup = \markup {
      \column {
        \vspace #0
        \fill-line {
          %\bold \fromproperty #'header:instrument
          \override #`(direction . ,UP)
          \dir-column {         
            \center-align \fontsize #3
            \fromproperty #'header:subtitle %% User-defined field
            \vspace #0.4
            \center-align \fontsize #6 { 
            \fromproperty #'header:title
            }
            \vspace #0.4
            \center-align \fontsize #4 
            \fromproperty #'header:composer            
            
          }
          %\fromproperty #'header:composer
        }
        \vspace #2
      }
    }
  }
  
  \header { tagline = ##f }  
  

  
  \bookpart {
    \paper {
      
      
      
      bookpart-level-page-numbering = ##t
      %print-page-number = ##t
      %print-first-page-number = ##t
      oddHeaderMarkup = \markup \null
      evenHeaderMarkup = \markup \null
      oddFooterMarkup = \markup {
        \fill-line {
          \if \should-print-page-number
          \fromproperty #'page:page-number-string
        }
      }
      evenFooterMarkup = \oddFooterMarkup
    }
   

    
    \score {
      <<
        \new PianoStaff <<
          \new Staff = "upper" \oneA
          \new Staff = "lower" \oneB
        >> 
      >>
      \layout{}
      \midi { }
    }   
    
    \score {
      <<
        \new PianoStaff <<
          \new Staff = "upper" \twoA
          \new Staff = "lower" \twoB
        >> 
      >>
      \layout{}
      \midi { }
    }   
    
  }  
  

}

  


