\version "2.18.2"
% -*- master: score.ly;

%#(set-default-paper-size "letterlandscape")
%#(set-default-paper-size "letter" 'landscape)

global = {
  \time 6/8
  \key g \major
  \clef bass
}

oneB = \relative c {
  \global
  \compressMMRests
  <g d'>2.
  <b g'>
  <a fis'>
  <d-4 fis-1>
  <a e'>4.( d) 
  <b fis'>( e) 
  g,( a) d,~d8 r r 
  g2. b4.( e) a,2. ees4.( d)
  g( c,~ c) <ees c'>
  
  d2. g4.~g8 r r
  
  e2.\p d c g' a c4. a d,2.~d4. d' 
  g,2. e c4.(~c4 a'8) d,4 r8 r4 r8 
  
  <g d'>2.
  <b g'>
  <a fis'>
  <d-4 fis-1>
  <a e'>4.( d) 
  <b fis'>( e) 
  g,( a) d,~d8 r r 
  g2. b4.( e) a,2. ees4.( d)
  g( c,~ c) <ees c'>
  
  d2. g4.~g8 r r
 
 
}